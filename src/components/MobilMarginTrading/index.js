/**
 * Developer : Devang parekh (19-2-2019)
 * Reason : Export all the component for trading and wallet component, therefor easily used in other route where imports
 */

//Import necessary components
export * from './Trading'
export * from './TradingHistory' // added by devang parekh for margin trading history (6-3-2019)
export * from './Wallet';
export * from './Ledger';
export * from './LeverageReport';
export * from './ProfitLossReport';
export * from './OpenPositionReport';
// Export necessary components
/* export {
  TradingComponents,
  WalletComponents
}; */
